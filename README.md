# RTX Tetris

A little demo where I played around with DXR in D3D12 to get a handle on what the new api looked like, and how it felt combining it with a traditional rasterization pipeline.

All in all its nothing special, but had a lot of fun with it. The code quality is somewhere between not good, and terrible, but this mainly served as a fun test bed for me.

![alt text](https://gitlab.com/markypooch/rtxetris/-/blob/master/rtxetris.PNG?raw=true)
![alt text](https://gitlab.com/markypooch/rtxetris/-/blob/master/rtxetrisbb.PNG?raw=true)
