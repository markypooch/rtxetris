#pragma once
#include "DX12Graphics.h"

template<typename T>
class ConstantBufferHelper
{
public:
	ConstantBufferHelper(DX12Graphics* context, uint32_t numberOfBuffers) {

		for (int i = 0; i < numberOfBuffers; i++) {
			this->_buffers.push_back(context->CreateConstantBuffer(sizeof(T)));
		}

		_currentBuffer = 0;
		_context = context;
	};
	~ConstantBufferHelper() {
		for (int i = 0; i < this->_buffers.size(); i++) {
			//this->_buffers[i]->pBuffer->Release();
			//delete this->_buffers[i];
		}
	};
public:
	T type;
	ConstantBuffer* GetBuffer() {
		return _buffers[_currentBuffer];
	}
	ConstantBuffer* Update(unsigned int bindIndex) {
		_context->SetData(this->_buffers[_currentBuffer], &type, sizeof(T));
		ConstantBuffer* buffer = _buffers[_currentBuffer];

		_currentBuffer++;
		if (_currentBuffer >= _buffers.size())
			_currentBuffer = 0;

		return buffer;
	};

private:
	uint32_t _currentBuffer;
	std::vector<ConstantBuffer*> _buffers;
	DX12Graphics* _context;
};