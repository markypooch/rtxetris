#define STB_IMAGE_IMPLEMENTATION

#include "DX12Graphics.h"
#include "d3dx12.h"
#include "stb_image.h"

DX12Graphics::DX12Graphics() {
	resourcesBound = 0;
}

void DX12Graphics::Initialize(HWND hWnd, unsigned int width, unsigned int height) {

	CreateDXGIFactory1(IID_PPV_ARGS(&factory));
	D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_12_0, IID_PPV_ARGS(&device));

	D3D12_COMMAND_QUEUE_DESC cmdQueueDesc = {};
	cmdQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;

	device->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&cmdQueue));

	DXGI_SWAP_CHAIN_DESC swapDesc = {};
	swapDesc.BufferCount = 3;
	swapDesc.BufferDesc.Height = height;
	swapDesc.BufferDesc.Width = width;
	swapDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapDesc.OutputWindow = hWnd;
	swapDesc.SampleDesc.Count = 1;
	swapDesc.SampleDesc.Quality = 0;
	swapDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapDesc.Windowed = true;

	IDXGISwapChain* swapChain1;
	factory->CreateSwapChain(cmdQueue, &swapDesc, &swapChain1);

	swapChain = static_cast<IDXGISwapChain3*>(swapChain1);

	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
	rtvHeapDesc.NumDescriptors = 128;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;

	D3D12_DESCRIPTOR_HEAP_DESC srvHeapDesc = {};
	srvHeapDesc.NumDescriptors = 32000;
	srvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;

	device->CreateDescriptorHeap(&srvHeapDesc, IID_PPV_ARGS(&cbvSrvUavNonShaderVisibleHeap));

	srvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	device->CreateDescriptorHeap(&srvHeapDesc, IID_PPV_ARGS(&cbvSrvUavShaderVisibleHeap));

	device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&rtvHeap));
	rtvDescriptorHandleOffset = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	cbvSrvUavDescriptorHandleIncrementSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
	dsvHeapOffset = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);

	//rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&rtvSVHeap));

	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc = {};
	dsvHeapDesc.NumDescriptors = 128;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;

	device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(&dsvHeap));

	for (int i = 0; i < BUFFER_COUNT; i++) {

		swapChain->GetBuffer(i, IID_PPV_ARGS(&rtvTextures[i]));
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			rtvHeap->GetCPUDescriptorHandleForHeapStart(),
			i,
			rtvDescriptorHandleOffset
		);

		device->CreateRenderTargetView(rtvTextures[i], nullptr, rtvHandle);
		device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&cmdAllocator[i]));

		fenceValue[i] = 0;
		device->CreateFence(fenceValue[i], D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence[i]));
	}

	fenceEvent = CreateEvent(nullptr, false, false, "");
	device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, cmdAllocator[0], nullptr, IID_PPV_ARGS(&cmdList));

	view = {};
	view.MinDepth = 0.1f;
	view.MaxDepth = 1.0f;
	view.TopLeftX = 0.0f;
	view.TopLeftY = 0.0f;
	view.Height = height;
	view.Width = width;

	rect = {};
	rect.bottom = height;
	rect.right = width;
	rect.left = 0.0f;
	rect.top = 0.0f;
}

void DX12Graphics::SetData(ConstantBuffer* cb, void* pData, unsigned int size) {
	memcpy(cb->pAddress, (void*)pData, size);
}

DepthBuffer* DX12Graphics::CreateDepthBuffer(unsigned int width, unsigned int height, DXGI_FORMAT format) {

	DepthBuffer* depth = new DepthBuffer();
	depth->shaderResource = new ShaderResource();

	D3D12_RESOURCE_DESC resourceDesc = {};
	resourceDesc.Alignment = 0;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.Format = format;
	resourceDesc.Height = height;
	resourceDesc.Width = width;
	resourceDesc.MipLevels = 1;
	resourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.SampleDesc.Quality = 0;

	device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		nullptr,
		IID_PPV_ARGS(&depth->shaderResource->pTexture));

	D3D12_DEPTH_STENCIL_VIEW_DESC depthDesc = {};
	depthDesc.Format = DXGI_FORMAT_D32_FLOAT;
	depthDesc.Texture2D.MipSlice = 0;
	depthDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	
	CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		dsvHeap->GetCPUDescriptorHandleForHeapStart(),
		dsvHeapIndex,
		dsvHeapOffset
	);

	device->CreateDepthStencilView(depth->shaderResource->pTexture, &depthDesc, cpuHandle);
	depth->dsvHeapIndex = dsvHeapIndex;
	depth->shaderResource->resourceState = D3D12_RESOURCE_STATE_DEPTH_WRITE;

	dsvHeapIndex++;

	CD3DX12_CPU_DESCRIPTOR_HANDLE srvCbvRTVDescriptorHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		cbvSrvUavHeapIndex,
		cbvSrvUavDescriptorHandleIncrementSize
	);
	

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	device->CreateShaderResourceView(depth->shaderResource->pTexture, &srvDesc, srvCbvRTVDescriptorHandle);

	cbvSrvUavHeapIndex++;

	return depth;
}

ShaderResource* DX12Graphics::CreateTexture(const char* fileName) {

	ShaderResource* shaderResource = new ShaderResource();
	
	int x, y;
	int channel;
	void* pData = stbi_load(fileName, &x, &y, &channel, 4);

	D3D12_RESOURCE_DESC resourceDesc = {};
	resourceDesc.Alignment = 0;
	resourceDesc.Width = x;
	resourceDesc.Height = y;
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.MipLevels = 1;
	resourceDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	resourceDesc.SampleDesc.Count = 1;
	resourceDesc.SampleDesc.Quality = 0;

	unsigned long long textureUploadSize = 0;
	device->GetCopyableFootprints(&resourceDesc, 0, 1, 0, nullptr, 0, 0, &textureUploadSize);

	device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(textureUploadSize),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&shaderResource->pTextureUpload));

	device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&resourceDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&shaderResource->pTexture));

	D3D12_SUBRESOURCE_DATA textureData = {};
	textureData.pData = pData;
	textureData.RowPitch = (x * channel);
	textureData.SlicePitch = (x * channel) * y;

	UpdateSubresources(cmdList, shaderResource->pTexture, shaderResource->pTextureUpload, 0, 0, 1, &textureData);
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(shaderResource->pTexture, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		cbvSrvUavHeapIndex,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	device->CreateShaderResourceView(shaderResource->pTexture, &srvDesc, srvHandle);
	shaderResource->heapIndex = cbvSrvUavHeapIndex;

	cbvSrvUavHeapIndex++;
	return shaderResource;
}

ConstantBuffer* DX12Graphics::CreateConstantBuffer(unsigned int byteWidth) {

	ConstantBuffer* cb = new ConstantBuffer();

	device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(1024 * 64),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&cb->pBuffer));

	CD3DX12_CPU_DESCRIPTOR_HANDLE cbHandle(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		cbvSrvUavHeapIndex,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	cb->heapIndex = cbvSrvUavHeapIndex;
	cbvSrvUavHeapIndex++;

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc = {};
	cbViewDesc.BufferLocation = cb->pBuffer->GetGPUVirtualAddress();
	cbViewDesc.SizeInBytes = byteWidth + (256 - byteWidth);

	device->CreateConstantBufferView(&cbViewDesc, cbHandle);
	cb->cbViewDesc = cbViewDesc;
	
	CD3DX12_RANGE range(0, 0);
	cb->pBuffer->Map(0, &range, (void**)&cb->pAddress);

	return cb;
}

RenderTarget* DX12Graphics::CreateRenderTarget(unsigned int width, unsigned int height, DXGI_FORMAT format) {

	RenderTarget* rt = new RenderTarget();
	rt->shaderResource = new ShaderResource();

	D3D12_RESOURCE_DESC resource = {};
	resource.Alignment = 0;
	resource.DepthOrArraySize = 1;
	resource.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resource.Height = height;
	resource.Width = width;
	resource.Format = format;
	resource.MipLevels = 0;
	resource.Flags = D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
	resource.SampleDesc.Count = 1;
	resource.SampleDesc.Quality = 0;

	D3D12_CLEAR_VALUE color = D3D12_CLEAR_VALUE();
	color.Color[0] = 0.0f;
	color.Color[1] = 0.0f;
	color.Color[2] = 0.0f;
	color.Color[3] = 0.0f;

	color.Format = format;

	device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&resource,
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		&color,
		IID_PPV_ARGS(&rt->shaderResource->pTexture));

	D3D12_RENDER_TARGET_VIEW_DESC rtvDesc = {};
	rtvDesc.Texture2D.MipSlice = 0;
	rtvDesc.Format = format;
	rtvDesc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
	
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		rtvHeap->GetCPUDescriptorHandleForHeapStart(),
		rtvHeapIndex,
		rtvDescriptorHandleOffset
	);

	device->CreateRenderTargetView(rt->shaderResource->pTexture, &rtvDesc, rtvHandle);
	rt->rtvHeapIndex = rtvHeapIndex;
	rt->rtvDesc = rtvDesc;
	rt->shaderResource->format = format;

	CD3DX12_CPU_DESCRIPTOR_HANDLE cbvSrvUavRtvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(),
		cbvSrvUavHeapIndex,
		cbvSrvUavDescriptorHandleIncrementSize
	);

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = format;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;

	device->CreateShaderResourceView(rt->shaderResource->pTexture, &srvDesc, cbvSrvUavRtvHandle);
	rt->shaderResource->heapIndex = cbvSrvUavHeapIndex;
	rt->shaderResource->resourceState = D3D12_RESOURCE_STATE_RENDER_TARGET;

	rtvHeapIndex++;
	cbvSrvUavHeapIndex++;
	
	return rt;
}

ID3D12RootSignature* DX12Graphics::CreateRootSignature(std::vector<D3D12_ROOT_PARAMETER> rootParameters, std::vector<D3D12_STATIC_SAMPLER_DESC> samplers) {

	ID3D12RootSignature* rootSignature;
	ID3DBlob* rootSigCode;

	CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc = CD3DX12_ROOT_SIGNATURE_DESC(rootParameters.size(), rootParameters.data(), samplers.size(), samplers.data(), D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
	D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &rootSigCode, nullptr);

	device->CreateRootSignature(0, rootSigCode->GetBufferPointer(), rootSigCode->GetBufferSize(), IID_PPV_ARGS(&rootSignature));

	return rootSignature;
}

D3D12_SHADER_BYTECODE DX12Graphics::CompileShader(LPCWSTR fileName, LPCSTR profile) {
	ID3DBlob* code, * error;
	D3DCompileFromFile(fileName, nullptr, nullptr, "main", profile, 0, 0, &code, &error);

	if (error) {
		const char* message = (const char*) error->GetBufferPointer();
		int x = 12;
	}

	D3D12_SHADER_BYTECODE byteCode = {};
	byteCode.BytecodeLength = code->GetBufferSize();
	byteCode.pShaderBytecode = code->GetBufferPointer();

	return byteCode;
}

void DX12Graphics::SetRenderTarget(std::vector<RenderTarget*> rtvs, DepthBuffer* depth) {
	if (rtvs.size() > 0) {

		int index = 0;
		for (auto const& rtv : rtvs) {
			CD3DX12_CPU_DESCRIPTOR_HANDLE rtvCPUHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
				rtvHeap->GetCPUDescriptorHandleForHeapStart(),
				rtv->rtvHeapIndex,
				rtvDescriptorHandleOffset
			);

			CD3DX12_CPU_DESCRIPTOR_HANDLE rtvSVCPUHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
				rtvSVHeap->GetCPUDescriptorHandleForHeapStart(),
				index,
				rtvDescriptorHandleOffset
			);

			device->CopyDescriptorsSimple(1, rtvSVCPUHandle, rtvCPUHandle, D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

			if (rtv->shaderResource->resourceState != D3D12_RESOURCE_STATE_RENDER_TARGET) {
				cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(rtv->shaderResource->pTexture, rtv->shaderResource->resourceState, D3D12_RESOURCE_STATE_RENDER_TARGET));
				rtv->shaderResource->resourceState = D3D12_RESOURCE_STATE_RENDER_TARGET;
			}

			index++;
		}

		if (depth->shaderResource->resourceState != D3D12_RESOURCE_STATE_DEPTH_WRITE) {
			cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(depth->shaderResource->pTexture, depth->shaderResource->resourceState, D3D12_RESOURCE_STATE_DEPTH_WRITE));
		}

		CD3DX12_CPU_DESCRIPTOR_HANDLE dsvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(dsvHeap->GetCPUDescriptorHandleForHeapStart(), depth->dsvHeapIndex, dsvHeapOffset);
		cmdList->OMSetRenderTargets(rtvs.size(), &rtvSVHeap->GetCPUDescriptorHandleForHeapStart(), true, &dsvHandle);
	}
	else {
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			rtvHeap->GetCPUDescriptorHandleForHeapStart(),
			frameIndex,
			rtvDescriptorHandleOffset
		);

		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(rtvTextures[frameIndex], D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

		float clearColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
		cmdList->OMSetRenderTargets(1, &rtvHandle, false, nullptr);
		cmdList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
	}
}

void DX12Graphics::ClearDepthStencil(DepthBuffer* depthBuffer) {

	CD3DX12_CPU_DESCRIPTOR_HANDLE dsvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		dsvHeap->GetCPUDescriptorHandleForHeapStart(),
		depthBuffer->dsvHeapIndex,
		dsvHeapOffset
	);

	if (depthBuffer->shaderResource->resourceState != D3D12_RESOURCE_STATE_DEPTH_WRITE) {
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(depthBuffer->shaderResource->pTexture, depthBuffer->shaderResource->resourceState, D3D12_RESOURCE_STATE_DEPTH_WRITE));
		depthBuffer->shaderResource->resourceState = D3D12_RESOURCE_STATE_DEPTH_WRITE;
	}

	cmdList->ClearDepthStencilView(dsvHandle, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
}

void DX12Graphics::ClearRenderTarget(std::vector<RenderTarget*> rtvs, const float* clearColor) {

	for (auto const& rtv : rtvs) {

		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle = CD3DX12_CPU_DESCRIPTOR_HANDLE(
			rtvHeap->GetCPUDescriptorHandleForHeapStart(),
			rtv->rtvHeapIndex,
			rtvDescriptorHandleOffset
		);

		cmdList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);
	}
}

void DX12Graphics::SetVertexBuffer(VertexBuffer* vb) {
	cmdList->IASetVertexBuffers(0, 1, &vb->vbView);
}

void DX12Graphics::SetIndexBuffer(IndexBuffer* ib) {
	cmdList->IASetIndexBuffer(&ib->ibView);
}

void DX12Graphics::Draw(unsigned int vtxCount, VertexBuffer* vb, ID3D12PipelineState* pso, ID3D12RootSignature* rootSignature) {
	cmdList->SetPipelineState(pso);
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	cmdList->DrawInstanced(vtxCount, 1, 0, 0);

	cbvSrvUavDrawOffset += resourcesBound;
	resourcesBound = 0;
}

void DX12Graphics::DrawIndexed(unsigned int idxCount, unsigned int idxStart, VertexBuffer* vb, IndexBuffer* ib, ID3D12PipelineState* pso, ID3D12RootSignature* rootSignature) {
	cmdList->SetPipelineState(pso);
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	cmdList->DrawIndexedInstanced(idxCount, 1, idxStart, 0, 0);

	cbvSrvUavDrawOffset += resourcesBound;
	resourcesBound = 0;
}

void DX12Graphics::SetRootSignature(ID3D12RootSignature* rootSignature, unsigned int parameterCount) {

	cmdList->SetGraphicsRootSignature(rootSignature);
	for (int i = 0; i < parameterCount; i++) {

		CD3DX12_GPU_DESCRIPTOR_HANDLE gpuHandle = CD3DX12_GPU_DESCRIPTOR_HANDLE(
			cbvSrvUavShaderVisibleHeap->GetGPUDescriptorHandleForHeapStart(),
			cbvSrvUavDrawOffset,
			cbvSrvUavDescriptorHandleIncrementSize
		);

		cmdList->SetGraphicsRootDescriptorTable(i, gpuHandle);
	}
}

void DX12Graphics::WaitForResourceLoads() {

	cmdList->Close();
	ID3D12CommandList* c[] = { cmdList };
	cmdQueue->ExecuteCommandLists(1, c);

	fenceValue[0]++;
	cmdQueue->Signal(fence[0], fenceValue[0]);

	fence[frameIndex]->SetEventOnCompletion(fenceValue[frameIndex], fenceEvent);
	WaitForSingleObject(fenceEvent, INFINITE);
}

void DX12Graphics::BeginFrame() {
	frameIndex = swapChain->GetCurrentBackBufferIndex();
	if (fence[frameIndex]->GetCompletedValue() < fenceValue[frameIndex]) {
		fence[frameIndex]->SetEventOnCompletion(fenceValue[frameIndex], fenceEvent);
		WaitForSingleObject(fenceEvent, INFINITE);
	}

	cmdAllocator[frameIndex]->Reset();
	cmdList->Reset(cmdAllocator[frameIndex], nullptr);

	cmdList->RSSetViewports(1, &view);
	cmdList->RSSetScissorRects(1, &rect);

	ID3D12DescriptorHeap* heaps[] = { cbvSrvUavShaderVisibleHeap };
	cmdList->SetDescriptorHeaps(1, heaps);
}

void DX12Graphics::EndFrame() {
	cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(rtvTextures[frameIndex], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));
	cmdList->Close();

	ID3D12CommandList* c[] = { cmdList };
	cmdQueue->ExecuteCommandLists(1, c);

	fenceValue[frameIndex]++;
	cmdQueue->Signal(fence[frameIndex], fenceValue[frameIndex]);

	swapChain->Present(0, 0);

	if ((frameIndex + 1) % 3 == 0)
		cbvSrvUavDrawOffset = 0;
}