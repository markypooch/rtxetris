#include "Model.h"
#include "ConstantBufferHelper.h"
#include <d3d12shader.h>
#include <unordered_map>
#include <fstream>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/pbrmaterial.h>

static std::unordered_map<std::string, PassModelData*> models;

struct Binding {
	unsigned int firstRegister;
	unsigned int count;
	unsigned int offset;
};

//Prevent exporting to public symbol table during building of translation unit 
namespace {
	std::wstring s2ws(const std::string& s)
	{
		int len;
		int slength = (int)s.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;
		return r;
	}


	std::vector<std::string> stringSplit(std::string delimiter, std::string s) {

		std::vector<std::string> tokens;

		size_t pos = 0;
		std::string token;
		while ((pos = s.find(delimiter)) != std::string::npos) {
			token = s.substr(0, pos);
			tokens.push_back(token);
			s.erase(0, pos + delimiter.length());

			if ((pos = s.find(delimiter)) == std::string::npos) {
				token = s.substr(0, pos);
				tokens.push_back(token);
			}
		}

		return tokens;
	}

	void AddModelPointLight(PassModelData& model, std::vector<std::string>& materialAttributes, const float* position, unsigned int screenWidth, unsigned int screenHeight) {

		PointLight pointLight = {
			std::stof(materialAttributes[1]), std::stof(materialAttributes[2]), std::stof(materialAttributes[3]), 1.0f,
			position[0],                      position[1],                      position[2] * -1.0f,                      position[3],
			std::stof(materialAttributes[4]),
			screenWidth,
			screenHeight
		};

		model.pointLights.push_back(pointLight);
	}

	void ReadPointLights(std::string magmaModelName, PassModelData& model, unsigned int screenWidth, unsigned int screenHeight) {
		std::ifstream f = std::ifstream(magmaModelName, std::fstream::in);

		std::string line;
		while (std::getline(f, line)) {
			if (line.size() < 4) continue;

			if (line[0] == 'o' && line[2] == 'P' && line[3] == 'L') {
				std::vector<std::string> lineAttributes = stringSplit(" ", line);
				std::vector<std::string> pointLightAttributes = stringSplit(":", lineAttributes[1]);

				std::getline(f, line);

				std::vector<std::string> pointLightPositionString = stringSplit(" ", line);

				float pointLightPosition[4] = { std::stof(pointLightPositionString[1]), std::stof(pointLightPositionString[2]), std::stof(pointLightPositionString[3]), 1.0f };

				AddModelPointLight(model, pointLightAttributes, pointLightPosition, 3840, 2160);
			}
		}
	}

	void PrepassShaderArguments(std::unordered_map<D3D_SHADER_INPUT_TYPE, Binding>& bindings, 
		D3D12_SHADER_BYTECODE byteCode,
		std::unordered_map<D3D_SHADER_INPUT_TYPE, unsigned int>& count,
		std::unordered_map<D3D_SHADER_INPUT_TYPE, unsigned int>& firstRegister,
		std::unordered_map<std::string, bool>& bufferNames,
		unsigned int& argumentCount) {

		ID3D12ShaderReflection* reflection;

		D3DReflect(byteCode.pShaderBytecode, byteCode.BytecodeLength, IID_PPV_ARGS(&reflection));
		D3D_SHADER_INPUT_TYPE lastType = D3D_SIT_SAMPLER;

		unsigned int it = 0;
		for (int i = 0; i < 256; i++) {

			D3D12_SHADER_INPUT_BIND_DESC inputDesc = {};
			reflection->GetResourceBindingDesc(i, &inputDesc);

			if (!inputDesc.Name) {

				D3D12_DESCRIPTOR_RANGE_TYPE type;
				switch (lastType) {
				case D3D_SIT_TEXTURE:
					type = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
					break;
				case D3D_SIT_CBUFFER:
					type = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
					break;
				}

				if (bindings.count(lastType) == 0) {
					bindings[lastType] = Binding();
					bindings[lastType].count = count[lastType];
					bindings[lastType].firstRegister = firstRegister[lastType];
				}
				else {
					bindings[lastType].count = count[lastType];
				}

				break;
			}

			if (inputDesc.Type == D3D_SIT_SAMPLER)
				continue;

			if (bufferNames.find(inputDesc.Name) != bufferNames.end())
				continue;

			if (count.find(inputDesc.Type) == count.end()) {
				firstRegister[inputDesc.Type] = inputDesc.BindPoint;
			}

			count[inputDesc.Type]++;
			bufferNames[inputDesc.Name] = true;

			if (inputDesc.Type != lastType && it > 0) {
				
				if (bindings.count(lastType) == 0) {
					bindings[lastType] = Binding();
					bindings[lastType].count = count[lastType];
					bindings[lastType].firstRegister = firstRegister[lastType];
				}
				else {
					bindings[lastType].count = count[lastType];
				}
				argumentCount = count[lastType];
			}

			lastType = inputDesc.Type;
			it++;
		}
	}

	bool CreateShadowPipeline(DX12Graphics* context, PassModelData* model, std::vector<std::string> materialAttributes) {
		if (materialAttributes.size() > 7 && materialAttributes[7] == "sns") {
			D3D12_INPUT_ELEMENT_DESC inputE[] = {
			{"POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"NORMAL",   0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"BITANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 40, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 56, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0}
			};

			D3D12_INPUT_LAYOUT_DESC inputLayout = {};
			inputLayout.NumElements = 5;
			inputLayout.pInputElementDescs = inputE;

			D3D12_SHADER_BYTECODE vsByteCode = context->CompileShader(L"Shaders/Shadows/ShadowMapWrite_NoSkinningVS.hlsl", "vs_5_0");
			D3D12_SHADER_BYTECODE psByteCode = context->CompileShader(L"Shaders/Shadows/ShadowMapWrite_NoSkinningPS.hlsl", "ps_5_0");

			D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
			psoDesc.InputLayout = inputLayout;
			psoDesc.VS = vsByteCode;
			psoDesc.PS = psByteCode;


			if (model->shadowPipeline) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	void CreatePipeline(DX12Graphics* context, PassSubset* subset, std::vector<std::string> materialAttributes) {
		
		D3D12_INPUT_ELEMENT_DESC inputE[] = {
			{"POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"NORMAL",   0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"BITANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 40, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
			{"TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 56, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0}
		};

		D3D12_INPUT_LAYOUT_DESC inputLayout = {};
		inputLayout.NumElements = 5;
		inputLayout.pInputElementDescs = inputE;

		auto vsShaderName = L"Shaders/" + s2ws(std::string(materialAttributes.at(1) + "VS.hlsl"));
		auto psShaderName = L"Shaders/" + s2ws(std::string(materialAttributes.at(1) + "PS.hlsl"));

		int index                = materialAttributes.at(1).find_last_of("_");
		std::string shaderPrefix = materialAttributes.at(1).substr(0, index);
		unsigned int numRTV      = (shaderPrefix == "GBuffer") ? 4 : 1;

		LPCWSTR lpVsShaderName = vsShaderName.c_str();
		LPCWSTR lpPsShaderName = psShaderName.c_str();
		
		D3D12_SHADER_BYTECODE vsByteCode = context->CompileShader(lpVsShaderName, "vs_5_0");
		D3D12_SHADER_BYTECODE psByteCode = context->CompileShader(lpPsShaderName, "ps_5_0");

		D3D12_ROOT_PARAMETER rootParameter;
		std::unordered_map<D3D_SHADER_INPUT_TYPE, unsigned int> count;
		std::unordered_map<D3D_SHADER_INPUT_TYPE, unsigned int> firstRegister;
		std::unordered_map<std::string, bool> bufferNames;
		unsigned int argumentCount = 0;

		std::unordered_map<D3D_SHADER_INPUT_TYPE, Binding> bindings;

		PrepassShaderArguments(bindings, vsByteCode, count, firstRegister, bufferNames, argumentCount);
		PrepassShaderArguments(bindings, psByteCode, count, firstRegister, bufferNames, argumentCount);

		std::vector<D3D12_DESCRIPTOR_RANGE> ranges;
		unsigned int offsetInTable = 0;
		for (const auto& binding : bindings) {
			D3D12_DESCRIPTOR_RANGE range = {};
			range.BaseShaderRegister = binding.second.firstRegister;
			range.NumDescriptors = binding.second.count;
			range.OffsetInDescriptorsFromTableStart = offsetInTable;
			range.RegisterSpace = 0;
			
			D3D12_DESCRIPTOR_RANGE_TYPE type;
			switch (binding.first) {
			case D3D_SIT_TEXTURE:
				type = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
				break;
			case D3D_SIT_CBUFFER:
				type = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
				break;
			}

			range.RangeType = type;
			ranges.push_back(range);

			offsetInTable += binding.second.count;
		}

		D3D12_ROOT_DESCRIPTOR_TABLE table = {};
		table.NumDescriptorRanges = ranges.size();
		table.pDescriptorRanges = ranges.data();

		rootParameter.DescriptorTable = table;
		rootParameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
		rootParameter.ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

		D3D12_STATIC_SAMPLER_DESC samplerDesc;
		samplerDesc.AddressU = samplerDesc.AddressV = samplerDesc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
		samplerDesc.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
		samplerDesc.Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
		samplerDesc.MaxAnisotropy = 0;
		samplerDesc.MinLOD = 0.0f;
		samplerDesc.MaxLOD = D3D12_FLOAT32_MAX;
		samplerDesc.MipLODBias = 0;
		samplerDesc.RegisterSpace = 0;
		samplerDesc.ShaderRegister = 0;
		samplerDesc.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

		ID3DBlob* rootSigCode;
		CD3DX12_ROOT_SIGNATURE_DESC rootDesc = CD3DX12_ROOT_SIGNATURE_DESC(1, &rootParameter, 1, &samplerDesc, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

		D3D12SerializeRootSignature(&rootDesc, D3D_ROOT_SIGNATURE_VERSION_1, &rootSigCode, nullptr);
		context->device->CreateRootSignature(0, rootSigCode->GetBufferPointer(), rootSigCode->GetBufferSize(), IID_PPV_ARGS(&subset->rootSignature));

		CD3DX12_BLEND_DESC blendDesc(D3D12_DEFAULT);
		if (materialAttributes.size() > 3 && materialAttributes[3] == "ba") {
			blendDesc.RenderTarget[0].BlendEnable = true;
			blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
			blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
			blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ONE;
			blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
			blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
			blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
		}
		else {
			blendDesc.RenderTarget[0].BlendEnable = false;
		}
	
		CD3DX12_DEPTH_STENCIL_DESC depthStencilDesc(D3D12_DEFAULT);
		if (materialAttributes.size() > 4 && materialAttributes[4] == "dd") {
			depthStencilDesc.DepthEnable = false;
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_NEVER;
			depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		}

		if (materialAttributes.size() > 4 && materialAttributes[4] == "df") {
			depthStencilDesc.DepthEnable = true;
			depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
			depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
		}

		CD3DX12_RASTERIZER_DESC rasterizerStateDesc(D3D12_DEFAULT);
		if (materialAttributes.size() > 5 && materialAttributes[5] == "cff") {
			rasterizerStateDesc.CullMode = D3D12_CULL_MODE_FRONT;
			rasterizerStateDesc.FillMode = D3D12_FILL_MODE_SOLID;
		}

		D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
		psoDesc.pRootSignature = subset->rootSignature;
		psoDesc.InputLayout = inputLayout;
		psoDesc.VS = vsByteCode;
		psoDesc.PS = psByteCode;
		psoDesc.NumRenderTargets = numRTV;
		psoDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
		
		for (int i = 0; i < numRTV; i++) {
			psoDesc.RTVFormats[i] = DXGI_FORMAT_R32G32B32A32_FLOAT;
		}

		psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		psoDesc.BlendState = blendDesc;
		psoDesc.DepthStencilState = depthStencilDesc;
		psoDesc.RasterizerState = rasterizerStateDesc;
		psoDesc.SampleDesc.Count = 1;
		psoDesc.SampleDesc.Quality = 0;
		psoDesc.SampleMask = 0xffffffff;

		context->device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&subset->pipeline));
	}
}

PassModelData* Model::LoadAI(DX12Graphics& context, std::string magmaModelName, unsigned int screenWidth, unsigned int screenHeight, std::vector<D3D12_ROOT_PARAMETER> rootParams) {

	if (models.count(magmaModelName) == 0) {

		Assimp::Importer importer;

		const aiScene* scene = importer.ReadFile(magmaModelName, aiProcess_ConvertToLeftHanded | aiProcess_CalcTangentSpace | aiProcess_ForceGenNormals | aiProcess_FindDegenerates | aiProcess_FindInvalidData | aiProcess_PreTransformVertices);

		PassModelData* magmaModel = new PassModelData();
		magmaModel->subsets.resize(scene->mNumMeshes);
		std::vector<PassSubset> subset;

		std::string diffuseTextureName, diffuseTexture;
		std::string normalTextureName, normalTexture;
		unsigned int indicesOffset = 0;

		std::vector<DefaultVertex> vbVertices;
		std::vector<unsigned int> ibIndices;

		ReadPointLights(magmaModelName, *magmaModel, 800, 600);
		bool createdShadowPipeline = false;
		for (int i = 0; i < scene->mNumMeshes; i++) {
			aiMesh* mesh = scene->mMeshes[i];

			auto material = scene->mMaterials[mesh->mMaterialIndex];

			aiString name;
			aiGetMaterialString(material, AI_MATKEY_NAME, &name);
			magmaModel->subsets[i].name = std::string(name.C_Str());

			std::vector<std::string> materialAttributes = stringSplit(":", name.C_Str());

			std::vector<DefaultVertex> vertices;
			vertices.resize(mesh->mNumVertices);
			for (int j = 0; j < mesh->mNumVertices; j++) {
				vertices[j].pos[0] = mesh->mVertices[j].x;
				vertices[j].pos[1] = mesh->mVertices[j].y;
				vertices[j].pos[2] = mesh->mVertices[j].z;
				vertices[j].pos[3] = 1.0;

				vertices[j].uvCoordinate[0] = (mesh->mTextureCoords[0]) ? mesh->mTextureCoords[0][j].x : 0.0f;
				vertices[j].uvCoordinate[1] = (mesh->mTextureCoords[0]) ? mesh->mTextureCoords[0][j].y : 0.0f;

				vertices[j].normal[0] = mesh->mNormals[j].x;
				vertices[j].normal[1] = mesh->mNormals[j].y;
				vertices[j].normal[2] = mesh->mNormals[j].z;
				vertices[j].normal[3] = 0.0;

				vertices[j].bitangent[0] = (mesh->mBitangents) ? mesh->mBitangents[j].x : 0.0f;
				vertices[j].bitangent[1] = (mesh->mBitangents) ? mesh->mBitangents[j].y : 0.0f;
				vertices[j].bitangent[2] = (mesh->mBitangents) ? mesh->mBitangents[j].z : 0.0f;
				vertices[j].bitangent[3] = 0.0;

				vertices[j].tangent[0] = (mesh->mTangents) ? mesh->mTangents[j].x : 0.0f;
				vertices[j].tangent[1] = (mesh->mTangents) ? mesh->mTangents[j].y : 0.0f;
				vertices[j].tangent[2] = (mesh->mTangents) ? mesh->mTangents[j].z : 0.0f;
				vertices[j].tangent[3] = 0.0;
			}

			std::vector<unsigned int> indices;

			if (i > 0) indicesOffset = ibIndices.size();
			for (int j = 0; j < mesh->mNumFaces; j++) {
				const aiFace& face = mesh->mFaces[j];
				if (face.mNumIndices == 3) {
					indices.push_back(face.mIndices[0] + indicesOffset);
					indices.push_back(face.mIndices[1] + indicesOffset);
					indices.push_back(face.mIndices[2] + indicesOffset);
				}
			}

			//subset[i].vertices = vertices;
			//subset[i].indices = indices;

			aiColor4D diffuse, ambient, emissive, specular;
			aiGetMaterialColor(material, AI_MATKEY_COLOR_DIFFUSE, &diffuse);

			float metallic, roughness;
			aiGetMaterialFloat(material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLIC_FACTOR, &metallic);
			aiGetMaterialFloat(material, AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_ROUGHNESS_FACTOR, &roughness);

			//aiGetMaterialColor(material, AI_MATKEY_COLOR_AMBIENT, &ambient);
			//aiGetMaterialColor(material, AI_MATKEY_COLOR_SPECULAR, &specular);
			//aiGetMaterialColor(material, AI_MATKEY_COLOR_EMISSIVE, &emissive);

			CreatePipeline(&context, &magmaModel->subsets[i], materialAttributes);

			if (!createdShadowPipeline) {
				//createdShadowPipeline = CreateShadowPipeline(&context, magmaModel, materialAttributes);
			}

			magmaModel->subsets[i].cbMaterial = new ConstantBufferHelper<Material>(&context, 3);
			magmaModel->subsets[i].cbMaterial->type.diffuse[0] = diffuse.r;
			magmaModel->subsets[i].cbMaterial->type.diffuse[1] = diffuse.g;
			magmaModel->subsets[i].cbMaterial->type.diffuse[2] = diffuse.b;
			magmaModel->subsets[i].cbMaterial->type.diffuse[3] = diffuse.a;

			magmaModel->subsets[i].cbMaterial->type.metallic = metallic;
			magmaModel->subsets[i].cbMaterial->type.roughness = roughness;

			/*
			magmaModel->subsets[i].cbMaterial->type.ambient[0] = ambient.r;
			magmaModel->subsets[i].cbMaterial->type.ambient[1] = ambient.g;
			magmaModel->subsets[i].cbMaterial->type.ambient[2] = ambient.b;
			magmaModel->subsets[i].cbMaterial->type.ambient[3] = ambient.a;

			magmaModel->subsets[i].cbMaterial->type.emissive[0] = emissive.r;
			magmaModel->subsets[i].cbMaterial->type.emissive[1] = emissive.g;
			magmaModel->subsets[i].cbMaterial->type.emissive[2] = emissive.b;
			magmaModel->subsets[i].cbMaterial->type.emissive[3] = emissive.a;

			magmaModel->subsets[i].cbMaterial->type.specular[0] = specular.r;
			magmaModel->subsets[i].cbMaterial->type.specular[1] = specular.g;
			magmaModel->subsets[i].cbMaterial->type.specular[2] = specular.b;
			magmaModel->subsets[i].cbMaterial->type.specular[3] = specular.a;
			*/

			magmaModel->subsets[i].buffers[1] = magmaModel->subsets[i].cbMaterial->Update(-1);

			aiString Path;
			material->GetTexture(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_BASE_COLOR_TEXTURE, &Path);
			if (Path.length > 0) {
				diffuseTexture = std::string(Path.C_Str());
				size_t textureNameIndex = diffuseTexture.find_last_of('\\') + 1;

				diffuseTextureName = diffuseTexture.substr(textureNameIndex, diffuseTexture.size() - textureNameIndex);
				diffuseTextureName = "Assets/Textures/" + diffuseTextureName;

				magmaModel->subsets[i].textures[0] = context.CreateTexture(diffuseTextureName.c_str());
			}

			material->GetTexture(AI_MATKEY_GLTF_PBRMETALLICROUGHNESS_METALLICROUGHNESS_TEXTURE, &Path);
			if (Path.length > 0) {
				normalTexture = std::string(Path.C_Str());
				size_t textureNameIndex = normalTexture.find_last_of('\\') + 1;

				normalTextureName = normalTexture.substr(textureNameIndex, normalTexture.size() - textureNameIndex);
				normalTextureName = "Assets/Model/" + normalTextureName;

				magmaModel->subsets[i].textures[1] = context.CreateTexture(normalTextureName.c_str());
			}

			magmaModel->subsets[i].indexSize = indices.size();
			magmaModel->subsets[i].indexOffset = indicesOffset;

			vbVertices.insert(vbVertices.end(), vertices.begin(), vertices.end());
			ibIndices.insert(ibIndices.end(), indices.begin(), indices.end());

			magmaModel->subsets[i].vertexBuffer = context.CreateVertexBuffer(vbVertices.data(), sizeof(DefaultVertex)* vbVertices.size(), sizeof(DefaultVertex));
			magmaModel->subsets[i].indexBuffer = context.CreateIndexBuffer(true, ibIndices.data(), sizeof(unsigned int) * ibIndices.size());

			vbVertices.clear();
			ibIndices.clear();
		}
		
		models[magmaModelName] = magmaModel;

		return models[magmaModelName];
	}
	else {
		return models[magmaModelName];
	}
}
