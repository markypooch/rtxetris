#pragma once
#include "DX12Graphics.h"
#include <map>
#include "ConstantBufferHelper.h"

struct Material {
	float diffuse[4];
	float metallic;
	float roughness;
};

struct PassSubset {

	std::string name;
	unsigned int indexOffset;
	unsigned int indexSize;
	std::map<unsigned int, ShaderResource*> textures;
	std::map<unsigned int, ConstantBuffer*> buffers;

	VertexBuffer* vertexBuffer;
	IndexBuffer* indexBuffer;

	ID3D12RootSignature* rootSignature;
	ID3D12PipelineState* pipeline;
	ConstantBufferHelper<Material>* cbMaterial;
};

struct PassModelData {
	
	std::vector<PassSubset> subsets;
	std::vector<PointLight> pointLights;
	ID3D12PipelineState* shadowPipeline;
};