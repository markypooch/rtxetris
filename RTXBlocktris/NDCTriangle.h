#pragma once
#include "DX12Graphics.h"
class NDCTriangle
{
public:
	NDCTriangle(DX12Graphics* context);
public:
	void Init();
	void Render();
private:
	float color[4];
	ConstantBuffer* cb[3];
	ShaderResource* resource;
	DX12Graphics* context;

	ID3D12RootSignature* rootSignature;
	VertexBuffer* vb;
	
	D3D12_SHADER_BYTECODE vsCode, psCode;
	D3D12_INPUT_LAYOUT_DESC inputLayout;
	
	ID3D12PipelineState* pso;
};

