#include "Stage.h"
#include "Matrix4x4.h"
#include <WinUser.h>

namespace {
	struct QuadVertex {
		float position[4];
		float texcoord[2];
	};
}

float rotY = 0.0f;
float transY = 0.0f;
void Stage::OnLoad() {
	blockPiece = new BlockPiece(context);
	blockPiece->Init(BlockType::STRAIGHT);

	reflect = context->CreateTexture("Assets/Model/lab.png");

	gbuffer.push_back(context->CreateRenderTarget(3840, 2160, DXGI_FORMAT_R32G32B32A32_FLOAT));
	gbuffer.push_back(context->CreateRenderTarget(3840, 2160, DXGI_FORMAT_R32G32B32A32_FLOAT));
	gbuffer.push_back(context->CreateRenderTarget(3840, 2160, DXGI_FORMAT_R32G32B32A32_FLOAT));
	gbuffer.push_back(context->CreateRenderTarget(3840, 2160, DXGI_FORMAT_R32G32B32A32_FLOAT));
	gbufferDepth = context->CreateDepthBuffer(3840, 2160, DXGI_FORMAT_R32_FLOAT);

	brightnessExtract = context->CreateRenderTarget(3840, 2160, DXGI_FORMAT_R8G8B8A8_UNORM);
	brightnessExtractDepth = context->CreateDepthBuffer(3840, 2160, DXGI_FORMAT_R32_FLOAT);

	bloom = context->CreateRenderTarget(3840, 2160, DXGI_FORMAT_R8G8B8A8_UNORM);
	bloomDepth = context->CreateDepthBuffer(3840, 2160, DXGI_FORMAT_R32_FLOAT);

	texture2D = context->CreateRenderTarget(3840, 2160, DXGI_FORMAT_R8G8B8A8_UNORM);
	texture2DDepth = context->CreateDepthBuffer(3840, 2160, DXGI_FORMAT_R32_FLOAT);

	cameraPositionCB = new ConstantBufferHelper<CameraPosition>(context, 3);

	view.Identity();
	view.Translate(0.0f, 0.0f, 10.0f);

	cameraPositionCB->type.cameraPosition[0] = 0.0f;
	cameraPositionCB->type.cameraPosition[1] = 0.0f;
	cameraPositionCB->type.cameraPosition[2] = 10.0f;
	cameraPositionCB->type.cameraPosition[3] = 1.0f;

	proj.CreatePerspectiveProjection(60*3.14/180, 3840 / 2160, 0.1f, 100.0f);

	wvpCB = new ConstantBufferHelper<wvp>(context, 3);

	QuadVertex vertices[] = {
		{-1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f},
		{-1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f},
		{ 1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 0.0f},
		{ 1.0f,  1.0f, 0.0f, 1.0f, 1.0f, 0.0f},
		{ 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 1.0f},
		{-1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f}
	};

	vb = context->CreateVertexBuffer(vertices, sizeof(QuadVertex) * 6, sizeof(QuadVertex));

	ID3DBlob* vsCode, *psCode;
	ID3DBlob* error;
	D3DCompileFromFile(L"Shaders/directionalVS.hlsl", nullptr, nullptr, "main", "vs_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &vsCode, &error);

	if (error) {
		const char* message = (const char*) error->GetBufferPointer();
		int x = 12;
	}

	D3DCompileFromFile(L"Shaders/directionalPS.hlsl", nullptr, nullptr, "main", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &psCode, &error);

	if (error) {
		const char* message = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	vsByteCode = {};
	vsByteCode.pShaderBytecode = vsCode->GetBufferPointer();
	vsByteCode.BytecodeLength = vsCode->GetBufferSize();

	psByteCode = {};
	psByteCode.BytecodeLength = psCode->GetBufferSize();
	psByteCode.pShaderBytecode = psCode->GetBufferPointer();

	D3D12_INPUT_ELEMENT_DESC inputElement[] = {
		{"POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0}
	};

	D3D12_INPUT_LAYOUT_DESC inputLayout = {};
	inputLayout.NumElements = 2;
	inputLayout.pInputElementDescs = inputElement;

	D3D12_DESCRIPTOR_RANGE range[2] = {};
	range[0].BaseShaderRegister = 0;
	range[0].NumDescriptors = 4;
	range[0].OffsetInDescriptorsFromTableStart = 0;
	range[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	range[0].RegisterSpace = 0;

	range[1].BaseShaderRegister = 0;
	range[1].NumDescriptors = 1;
	range[1].OffsetInDescriptorsFromTableStart = 4;
	range[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	range[1].RegisterSpace = 0;

	D3D12_ROOT_DESCRIPTOR_TABLE table = {};
	table.NumDescriptorRanges = 2;
	table.pDescriptorRanges = range;

	D3D12_ROOT_PARAMETER parameter = {};
	parameter.DescriptorTable = table;
	parameter.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	parameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;

	D3D12_STATIC_SAMPLER_DESC sampler;
	sampler.AddressU = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	sampler.AddressV = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	sampler.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
	sampler.MaxAnisotropy = 0;
	sampler.MinLOD = 0;
	sampler.MaxLOD = D3D12_FLOAT32_MAX;
	sampler.MipLODBias = 0.0f;
	sampler.RegisterSpace = 0;
	sampler.ShaderRegister = 0;
	sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	
	ID3DBlob* rootSigCode;
	CD3DX12_ROOT_SIGNATURE_DESC rootDescriptorDesc = CD3DX12_ROOT_SIGNATURE_DESC(1, &parameter, 1, &sampler, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
	D3D12SerializeRootSignature(&rootDescriptorDesc, D3D_ROOT_SIGNATURE_VERSION_1, &rootSigCode, nullptr);

	context->device->CreateRootSignature(0, rootSigCode->GetBufferPointer(), rootSigCode->GetBufferSize(), IID_PPV_ARGS(&rootSignature));

	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = inputLayout;
	psoDesc.pRootSignature = rootSignature;
	psoDesc.VS = vsByteCode;
	psoDesc.PS = psByteCode;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.SampleMask = 0xffffffff;
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);

	context->device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&pso));

	ID3DBlob* bevsCode, *bepsCode;
	D3DCompileFromFile(L"Shaders/BrightnessExtractVS.hlsl", nullptr, nullptr, "main", "vs_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &bevsCode, &error);

	if (error) {
		const char* message = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	D3DCompileFromFile(L"Shaders/BrightnessExtractPS.hlsl", nullptr, nullptr, "main", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &bepsCode, &error);

	if (error) {
		const char* message = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	beVsByteCode = {};
	beVsByteCode.pShaderBytecode = bevsCode->GetBufferPointer();
	beVsByteCode.BytecodeLength = bevsCode->GetBufferSize();

	bePsByteCode = {};
	bePsByteCode.BytecodeLength = bepsCode->GetBufferSize();
	bePsByteCode.pShaderBytecode = bepsCode->GetBufferPointer();

	D3D12_DESCRIPTOR_RANGE berange = {};
	berange.BaseShaderRegister = 0;
	berange.NumDescriptors = 1;
	berange.OffsetInDescriptorsFromTableStart = 0;
	berange.RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	berange.RegisterSpace = 0;

	D3D12_ROOT_DESCRIPTOR_TABLE betable = {};
	betable.NumDescriptorRanges = 1;
	betable.pDescriptorRanges = &berange;

	D3D12_ROOT_PARAMETER beparameter = {};
	beparameter.DescriptorTable = betable;
	beparameter.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	beparameter.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;

	ID3DBlob* berootSigCode;
	CD3DX12_ROOT_SIGNATURE_DESC berootDescriptorDesc = CD3DX12_ROOT_SIGNATURE_DESC(1, &beparameter, 1, &sampler, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
	D3D12SerializeRootSignature(&berootDescriptorDesc, D3D_ROOT_SIGNATURE_VERSION_1, &berootSigCode, nullptr);

	context->device->CreateRootSignature(0, berootSigCode->GetBufferPointer(), berootSigCode->GetBufferSize(), IID_PPV_ARGS(&brightnessExtractRootSignature));

	psoDesc = {};
	psoDesc.InputLayout = inputLayout;
	psoDesc.pRootSignature = brightnessExtractRootSignature;
	psoDesc.VS = beVsByteCode;
	psoDesc.PS = bePsByteCode;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.SampleMask = 0xffffffff;
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);

	context->device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&brightnessExtractPso));

	ID3DBlob* bvsCode, *bpsCode;
	D3DCompileFromFile(L"Shaders/BlurVS.hlsl", nullptr, nullptr, "main", "vs_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &bvsCode, &error);

	if (error) {
		const char* message = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	D3DCompileFromFile(L"Shaders/BlurPS.hlsl", nullptr, nullptr, "main", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &bpsCode, &error);

	if (error) {
		const char* message = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	bloomVsCode = {};
	bloomVsCode.pShaderBytecode = bvsCode->GetBufferPointer();
	bloomVsCode.BytecodeLength = bvsCode->GetBufferSize();

	bloomPsCode = {};
	bloomPsCode.BytecodeLength = bpsCode->GetBufferSize();
	bloomPsCode.pShaderBytecode = bpsCode->GetBufferPointer();

	psoDesc = {};
	psoDesc.InputLayout = inputLayout;
	psoDesc.pRootSignature = brightnessExtractRootSignature;
	psoDesc.VS = bloomVsCode;
	psoDesc.PS = bloomPsCode;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.SampleMask = 0xffffffff;
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);

	context->device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&bloomPso));

	ID3DBlob* tvsCode, *tpsCode;
	D3DCompileFromFile(L"Shaders/Texture2DPassthroughVS.hlsl", nullptr, nullptr, "main", "vs_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &tvsCode, &error);

	if (error) {
		const char* message = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	D3DCompileFromFile(L"Shaders/Texture2DPassthroughPS.hlsl", nullptr, nullptr, "main", "ps_5_0", D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, 0, &tpsCode, &error);

	if (error) {
		const char* message = (const char*)error->GetBufferPointer();
		int x = 12;
	}

	textureVsCode = {};
	textureVsCode.pShaderBytecode = tvsCode->GetBufferPointer();
	textureVsCode.BytecodeLength = tvsCode->GetBufferSize();

	texturePsCode = {};
	texturePsCode.BytecodeLength = tpsCode->GetBufferSize();
	texturePsCode.pShaderBytecode = tpsCode->GetBufferPointer();

	D3D12_BLEND_DESC blendDesc = {};
	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = 0xf;

	psoDesc = {};
	psoDesc.InputLayout = inputLayout;
	psoDesc.pRootSignature = brightnessExtractRootSignature;
	psoDesc.VS = textureVsCode;
	psoDesc.PS = texturePsCode;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.SampleMask = 0xffffffff;
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = blendDesc;
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);

	context->device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&texturePso));
}

void Stage::OnUpdate(float dt, const bool* keyPresses) {
	Matrix4x4 world;
	world.Identity();

	for (auto& subset : blockPiece->model->subsets) {
		subset.textures[3] = reflect;
	}

	rotY = 1.57f;
	transY += 0.00045f;
	world.RotateY(rotY);

	POINT point;
	GetCursorPos(&point);
	int posX, posY;

	posX = point.x;
	posY = point.y;

	bool resetX = false;
	bool resetY = false;
	if (point.x >= 3780 || point.x <= 0) {
		resetX = true;
		posX = 3840 / 2;
	}

	if (point.y >= 2100 || point.y <= 0) {
		resetY = true;
		posY = 2160 / 2;
	}

	if (resetX || resetY) {
		SetCursorPos(posX, posY);

		lastPoint.x = posX;
		lastPoint.y = posY;
	}

	Matrix4x4 rotX, rotY, rot;
	rotX.Identity();
	rotY.Identity();
	rot.Identity();

	if (!coldMouse) {

		float dx = (float) (point.x - lastPoint.x)/3840;
		float dy = (float) (point.y - lastPoint.y)/2160;

		if (dx > 0.01) {
			int ohshit = 12;
		}
		
		float maxradians_roty = 1.57f*dx;
		float maxradians_rotx = 1.57f*dy;

		currentRotY += maxradians_roty;
		currentRotX += maxradians_rotx;

		rotY.RotateY(currentRotY);
		rotX.RotateX(currentRotX);
	}


	float movementAmount = 0.00085;
	if (keyPresses[4]) {
		movementAmount = 0.00255;
	}

	if (keyPresses[0]) {
		currentPosZ -= movementAmount;
	}
	if (keyPresses[1]) {
		currentPosX += movementAmount;
	}
	if (keyPresses[2]) {
		currentPosZ += movementAmount;
	}
	if (keyPresses[3]) {
		currentPosX -= movementAmount;
	}

	Matrix4x4 rotFinal = rot.Multiply(rotX, rotY);

	view.Translate(currentPosX, 0.0f, currentPosZ);
	Matrix4x4 lo = view.Multiply(view, rotFinal);

	cameraPositionCB->type.cameraPosition[0] = currentPosX;
	cameraPositionCB->type.cameraPosition[1] = 0.0f;
	cameraPositionCB->type.cameraPosition[2] = currentPosZ;
	cameraPositionCB->type.cameraPosition[3] = 1.0f;

	wvpCB->type.world = world.GetData();
	wvpCB->type.vp = view.Multiply(lo, proj).GetData();

	lastPoint = point;
	coldMouse = false;
}

void Stage::OnDraw() {

	float clearColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	context->SetRenderTarget(gbuffer, gbufferDepth);
	context->ClearRenderTarget(gbuffer, clearColor);
	context->ClearDepthStencil(gbufferDepth);

	for (auto const& subset : blockPiece->model->subsets) {
	
		context->SetVertexBuffer(subset.vertexBuffer);
		context->SetIndexBuffer(subset.indexBuffer);

		context->SetRootSignature(subset.rootSignature, 1);
		context->BindBufferResource(0, wvpCB->Update(-1));
		context->BindBufferResource(2, cameraPositionCB->Update(-1));
		int offset = 0;
		for (auto const& buffer : subset.buffers) {
			context->BindBufferResource(buffer.first, buffer.second);
			if (buffer.first > offset)
				offset = buffer.first;
		}

		offset = 2;

		for (auto const& texture : subset.textures) {
			context->BindShaderResource(texture.first, texture.second);
		}

		context->DrawIndexed(subset.indexSize, subset.indexOffset, nullptr, nullptr, subset.pipeline, nullptr);
	}

	context->SetRenderTarget({ brightnessExtract }, brightnessExtractDepth);
	context->ClearRenderTarget({ brightnessExtract }, clearColor);
	context->ClearDepthStencil(brightnessExtractDepth);
	context->SetVertexBuffer(vb);

	context->SetRootSignature(brightnessExtractRootSignature, 1);
	context->BindShaderResource(0, gbuffer[3]->shaderResource);

	context->Draw(6, vb, brightnessExtractPso, nullptr);

	context->SetRenderTarget({ bloom }, bloomDepth);
	context->ClearRenderTarget({ bloom }, clearColor);
	context->ClearDepthStencil(bloomDepth);
	context->SetVertexBuffer(vb);

	context->SetRootSignature(brightnessExtractRootSignature, 1);
	context->BindShaderResource(0, brightnessExtract->shaderResource);

	context->Draw(6, vb, bloomPso, nullptr);

	context->SetRenderTarget({}, nullptr);
	context->SetVertexBuffer(vb);

	context->SetRootSignature(rootSignature, 1);
	context->BindShaderResource(0, gbuffer[0]->shaderResource);
	context->BindShaderResource(1, gbuffer[1]->shaderResource);
	context->BindShaderResource(2, gbuffer[2]->shaderResource);
	context->BindShaderResource(3, gbuffer[3]->shaderResource);
	context->BindBufferResource(4, cameraPositionCB->Update(-1));

	context->Draw(6, vb, pso, nullptr);

	context->SetRootSignature(brightnessExtractRootSignature, 1);
	context->BindShaderResource(0, bloom->shaderResource);

	context->Draw(6, vb, texturePso, nullptr);
}