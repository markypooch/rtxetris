Texture2D brightness : register(t0);
SamplerState samplerPoint : register(s0);

struct VertexOut {
    float4 position : SV_POSITION;
    float2 texcoord : TEXCOORD;
};

float4 main(VertexOut vOut) : SV_TARGET {
     float4 finalColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
   
     float offsetX = (float)1/(float)3840;
     float offsetY = (float)1/(float)2160;

     finalColor = brightness.Sample(samplerPoint, vOut.texcoord);
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX, vOut.texcoord.y+offsetY));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX, vOut.texcoord.y-offsetY));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX, vOut.texcoord.y-offsetY));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX, vOut.texcoord.y+offsetY));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY*2.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY*2.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*2.0f, vOut.texcoord.y+offsetY*2.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*2.0f, vOut.texcoord.y-offsetY*2.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*2.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*2.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*2.0f, vOut.texcoord.y-offsetY*2.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*2.0f, vOut.texcoord.y+offsetY*2.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY*3.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY*3.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*3.0f, vOut.texcoord.y+offsetY*3.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*3.0f, vOut.texcoord.y-offsetY*3.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*3.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*3.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*3.0f, vOut.texcoord.y-offsetY*3.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*3.0f, vOut.texcoord.y+offsetY*3.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY*4.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY*4.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*4.0f, vOut.texcoord.y+offsetY*4.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*4.0f, vOut.texcoord.y-offsetY*4.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*4.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*4.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*4.0f, vOut.texcoord.y-offsetY*4.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*4.0f, vOut.texcoord.y+offsetY*4.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY*5.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY*5.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*5.0f, vOut.texcoord.y+offsetY*5.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*5.0f, vOut.texcoord.y-offsetY*5.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*5.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*5.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*5.0f, vOut.texcoord.y-offsetY*5.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*5.0f, vOut.texcoord.y+offsetY*5.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY*6.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY*6.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*6.0f, vOut.texcoord.y+offsetY*6.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*6.0f, vOut.texcoord.y-offsetY*6.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*6.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*6.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*6.0f, vOut.texcoord.y-offsetY*6.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*6.0f, vOut.texcoord.y+offsetY*6.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY*7.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY*7.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*7.0f, vOut.texcoord.y+offsetY*7.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*7.0f, vOut.texcoord.y-offsetY*7.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*7.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*7.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*7.0f, vOut.texcoord.y-offsetY*7.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*7.0f, vOut.texcoord.y+offsetY*7.0f));

      finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y+offsetY*8.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x, vOut.texcoord.y-offsetY*8.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*8.0f, vOut.texcoord.y+offsetY*8.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*8.0f, vOut.texcoord.y-offsetY*8.0f));

     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*8.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*8.0f, vOut.texcoord.y));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x+offsetX*8.0f, vOut.texcoord.y-offsetY*8.0f));
     finalColor += brightness.Sample(samplerPoint, float2(vOut.texcoord.x-offsetX*8.0f, vOut.texcoord.y+offsetY*8.0f));     

     finalColor /= 61;

     return finalColor;
}

     
    