cbuffer Material : register(b1) {
    float4 color;
    unsigned int metallic;
    unsigned int roughness;
}

struct VertexOut {
    float4 position : SV_POSITION;
    float4 wPosition : POSITION;
    float2 tex      : TEXCOORD;
    float4 normal   : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent   : TANGENT;
};

struct GBuffer {
    float4 wPosition : SV_TARGET0;
    float4 normal    : SV_TARGET1;
    float4 material  : SV_TARGET2;
    float4 glow      : SV_TARGET3;
};

GBuffer main(VertexOut vOut) {
    GBuffer gBuffer = (GBuffer)0;

    gBuffer.wPosition = float4(vOut.wPosition.xyz, metallic);
    gBuffer.normal    = float4(vOut.normal.xyz, roughness);
    gBuffer.material     = color;
    gBuffer.glow         = float4(0.6f, 0.05f, 0.05f, 1.0f);

    return gBuffer;

}