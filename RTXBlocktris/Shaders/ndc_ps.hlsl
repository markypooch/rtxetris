Texture2D diffuse : register(t0);
SamplerState sampleState : register(s0);

cbuffer color : register(b0) {
    float4 colorRed;
}

struct VertexOut {
    float4 position : SV_POSITION;
    float2 tex      : TEXCOORD;
};

float4 main(VertexOut vOut) : SV_TARGET {
    return diffuse.Sample(sampleState, vOut.tex) + colorRed.z;
}