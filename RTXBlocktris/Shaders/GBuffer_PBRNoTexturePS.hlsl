Texture2D reflectionMap : register(t3);
SamplerState samplerSt : register(s0);

cbuffer Material : register(b1) {
    float4 color;
    float metallic;
    float roughness;
};

cbuffer CameraPos : register(b2) {
    float4 cameraPosition;
};


struct Dirlight {
    float4 diffuse;
    float4 ambient;
    float4 lightDir;
};

struct VertexOut {
    float4 position : SV_POSITION;
    float4 wPosition : POSITION;
    float2 tex      : TEXCOORD;
    float4 normal   : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent   : TANGENT;
};

struct GBuffer {
    float4 worldPosition : SV_TARGET0;
    float4 worldNormal   : SV_TARGET1;
    float4 material      : SV_TARGET2;
    float4 glow          : SV_TARGET3;
};

float4 blurx(float xOffset, float yOffset, float4 R) {
    
    float4 rSampleOne = float4(0.0f, 0.0f, 0.0f, 0.0f);
    for (int i = 0; i < 16; i++) {
        rSampleOne += reflectionMap.Sample(samplerSt, float2(R.x + xOffset*i+1,  (1-R.y) - yOffset * i+1))/8;
        rSampleOne += reflectionMap.Sample(samplerSt, float2(R.x - xOffset*i+1,  (1-R.y) - yOffset * i+1))/8;
        rSampleOne += reflectionMap.Sample(samplerSt, float2(R.x + xOffset*i+1,  (1-R.y) + yOffset * i+1))/8;
        rSampleOne += reflectionMap.Sample(samplerSt, float2(R.x - xOffset*i+1,  (1-R.y) + yOffset * i+1))/8;    
    }

    return rSampleOne/64;
}

GBuffer main(VertexOut vOut) {

    GBuffer gBuffer = (GBuffer)0;

    Dirlight light;
    light.diffuse  = float4(1.0f, 1.0f, 1.0f, 1.0f);
    light.ambient  = float4(0.0f, 0.0f, 0.0f, 0.0f);
    light.lightDir = float4(0.0f, 0.25f, -1.0f, 0.0f);

    float4 V = normalize(float4(vOut.wPosition.xyz, 1.0f)-cameraPosition);
    float4 R = normalize(reflect(V, -light.lightDir));

    float xOffset = (float) 1/(float)1200.0f;
    float yOffset = (float) 1/(float)480.0f;

    float4 rSampleOne = float4(0.0f, 0.0f, 0.0f, 0.0f);

    rSampleOne = blurx(xOffset, yOffset, R);

    gBuffer.worldPosition = float4(vOut.wPosition.xyz, metallic);
    gBuffer.worldNormal   = float4(vOut.normal.xyz, roughness);
    gBuffer.material      = color + (reflectionMap.Sample(samplerSt, float2(R.x,  (1-R.y)))/2);
    gBuffer.glow          = 0.0f;
 
    return gBuffer;
}