Texture2D roughnessMap : register(t0);
SamplerState samplerL : register(s0);

cbuffer Material : register(b1) {
    float4 color;
    float metallic;
    float roughness;
};

struct VertexOut {
    float4 position : SV_POSITION;
    float4 wPosition : POSITION;
    float2 tex      : TEXCOORD;
    float4 normal   : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent   : TANGENT;
};

struct GBuffer {
    float4 worldPosition : SV_TARGET0;
    float4 worldNormal   : SV_TARGET1;
    float4 material      : SV_TARGET2;
    float4 glow          : SV_TARGET3;
};

GBuffer main(VertexOut vOut) {

    GBuffer gBuffer = (GBuffer)0;

    float4 roughness = roughnessMap.Sample(samplerL, vOut.tex);

    gBuffer.worldPosition = float4(vOut.wPosition.xyz, metallic);
    gBuffer.worldNormal   = float4(vOut.normal.xyz, roughness.x);
    gBuffer.material      = color;
    gBuffer.glow          = 0.0f;
 
    return gBuffer;
}