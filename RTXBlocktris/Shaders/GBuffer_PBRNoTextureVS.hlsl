cbuffer wvp : register(b0) {
    matrix vp;
    matrix world;
}

struct VertexIn {
    float4 position  : POSITION;
    float2 tex       : TEXCOORD;
    float4 normal    : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent   : TANGENT;
};

struct VertexOut {
    float4 position  : SV_POSITION;
    float4 wPosition : POSITION;
    float2 tex       : TEXCOORD;
    float4 normal    : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent   : TANGENT;
};

VertexOut main(VertexIn vIn) {
    VertexOut vOut = (VertexOut)0;

    vOut.position  = mul(world, vIn.position);
    vOut.position  = mul(vp, vOut.position);

    vOut.wPosition  = mul(world, vIn.position);

    vOut.tex       = vIn.tex;
    vOut.normal    = mul(world, vIn.normal);
    vOut.bitangent = mul(world, vIn.bitangent);
    vOut.tangent   = mul(world, vIn.tangent);

    return vOut;
}