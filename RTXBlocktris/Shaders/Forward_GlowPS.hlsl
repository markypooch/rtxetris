cbuffer Material : register(b0) {
    unsigned int metallic;
    unsigned int roughness;
    float4 color;
}

struct VertexOut {
    float4 position : SV_POSITION;
    float2 tex      : TEXCOORD;
    float4 normal   : NORMAL;
    float4 bitangent : BITANGENT;
    float4 tangent   : TANGENT;
};

float4 main(VertexOut vOut)