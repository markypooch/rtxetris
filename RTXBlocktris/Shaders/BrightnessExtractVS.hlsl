struct VertexIn {
    float4 position : POSITION;
    float2 texcoord : TEXCOORD;
};

struct VertexOut {
    float4 position : SV_POSITION;
    float2 texcoord : TEXCOORD;
};

VertexOut main(VertexIn vIn) {
    VertexOut vOut = (VertexOut)0;
  
    vOut.position = vIn.position;
    vOut.texcoord = vIn.texcoord;

    return vOut;
}