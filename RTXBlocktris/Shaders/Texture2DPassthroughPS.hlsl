Texture2D colorMap : register(t0);
SamplerState samplerPoint : register(s0);

struct VertexOut {
    float4 position : SV_POSITION;
    float2 texcoord : TEXCOORD;
};

float4 main(VertexOut vOut) : SV_TARGET {
    return colorMap.Sample(samplerPoint, vOut.texcoord);
}