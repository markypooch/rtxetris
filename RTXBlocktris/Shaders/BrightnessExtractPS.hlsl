Texture2D material : register(t0);
SamplerState samplerPoint : register(s0);

struct VertexOut {
    float4 position : SV_POSITION;
    float2 texcoord : TEXCOORD;
};

float4 main(VertexOut vOut) : SV_TARGET {
    float4 color = material.Sample(samplerPoint, vOut.texcoord);
  
    if ((color.r + color.g + color.z) > 0.3) {
        return color;
    }
    return float4(0.0f, 0.0f, 0.0f, 0.0f);
}