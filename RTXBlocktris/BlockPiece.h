#pragma once
#include "DX12Graphics.h"
#include "ConstantBufferHelper.h"
#include "Matrix4x4.h"
#include "PassData.h"

struct wvp {
	Matrix4x4Data vp;
	Matrix4x4Data world;
};

enum BlockType {
	STRAIGHT,
	CUBE,
	ELL
};

class BlockPiece
{
public:
	BlockPiece(DX12Graphics* context);

	void Init(BlockType);
	void Update(float dt);

public:
	float posX, posY, PosZ;
	float rotX, rotY, rotZ;

	PassModelData* model;

	ConstantBufferHelper<wvp>* wvpConstantBuffer;
	DX12Graphics* context;
};

