#pragma once
#include "PassData.h"

template<typename U, typename V>
class Pass
{
public:
	Pass(DX12Graphics* context);
	~Pass();
public:
	U inputResource;
	V outputResource;

	void Init() {

	}

	void AddModel(PassModelData* model) {
		this->models.push_back(model);
	}
	void Render() {
		for (const auto& model : models) {
			context->SetVertexBuffer(model->vertexBuffer);
			context->SetIndexBuffer(model->indexBuffer);

			for (const auto& subset : model->subsets) {
				context->SetRootSignature(subset.rootSignature, 1);
				
				int offset = 0;
				for (const auto& buffer : subset.buffers) {
					context->BindResource(buffer.first, buffer.second);
					offset++;
				}

				for (const auto& texture : subset.textures) {
					context->BindResource(offset+texture.first, texture.second);
				}

				context->DrawIndexed(subset.indexSize, subset.indexOffset, nullptr, nullptr, subset.pipeline, subset.rootSignature);
			}
		}
	}

	void Transition() {

	}
private:
	DX12Graphics* context;
	std::vector<PassModelData*> models;
};

