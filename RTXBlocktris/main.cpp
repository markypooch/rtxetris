#include <Windows.h>
#include "DX12Graphics.h"
#include "NDCTriangle.h"
#include "IStage.h"
#include "Stage.h"
#define WIN32_LEAN_AND_MEAN

bool keyPresses[] = { false, false, false, false, false};

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		if (wParam == 'W') {
			keyPresses[0] = true;
		}
		if (wParam == 'A') {
			keyPresses[1] = true;
		}
		if (wParam == 'S') {
			keyPresses[2] = true;
		}
		if (wParam == 'D') {
			keyPresses[3] = true;
		}
		if (wParam == VK_SHIFT) {
			keyPresses[4] = true;
		}
		if (wParam == VK_ESCAPE) {
			PostQuitMessage(0);
		}
		break;
	case WM_KEYUP:
		if (wParam == 'W') {
			keyPresses[0] = false;
		}
		if (wParam == 'A') {
			keyPresses[1] = false;
		}
		if (wParam == 'S') {
			keyPresses[2] = false;
		}
		if (wParam == 'D') {
			keyPresses[3] = false;
		}
		if (wParam == VK_SHIFT) {
			keyPresses[4] = false;
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE pInstance, LPSTR lpCmdLine, int nCmdShow) {

	HWND hWnd;
	WNDCLASSEX win = {};
	win.cbSize = sizeof(WNDCLASSEX);
	win.hInstance = hInstance;
	win.lpfnWndProc = WndProc;
	win.style = CS_HREDRAW | CS_VREDRAW;
	win.hbrBackground = (HBRUSH)COLOR_WINDOW;
	win.lpszClassName = "Window";

	if (!RegisterClassEx(&win)) {
		return -1;
	}

	hWnd = CreateWindowEx(0, "Window", "RTXetris", WS_OVERLAPPEDWINDOW, 0, 0, 3840, 2160, 0, 0, hInstance, nullptr);
	ShowWindow(hWnd, nCmdShow);

	ShowCursor(false);

	DX12Graphics* graphics = new DX12Graphics();
	graphics->Initialize(hWnd, 3840, 2160);

	IStage* stage = new Stage(graphics);
	stage->OnLoad();

	//NDCTriangle* ndcTriangle = new NDCTriangle(graphics);
	//ndcTriangle->Init();

	graphics->WaitForResourceLoads();

	MSG msg = {};

	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {

			stage->OnUpdate(0.0f, keyPresses);

			graphics->BeginFrame();
			
			
			stage->OnDraw();

			graphics->EndFrame();
		}
	}

	return 0;
}