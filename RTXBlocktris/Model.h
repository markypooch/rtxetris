#pragma once
#include <string>
#include <map>
#include "GraphicsContext.h"
#include "ConstantBufferHelper.h"
#include "PassData.h"

class Model
{
public:
	Model(std::string fileName);
public:
	static PassModelData* LoadAI(DX12Graphics&, std::string magmaModelName, unsigned int screenWidth, unsigned int screenHeight, std::vector<D3D12_ROOT_PARAMETER>);
};

