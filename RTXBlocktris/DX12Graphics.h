#pragma once
#include <d3d12.h>
#include <dxgi1_4.h>
#include <d3dcompiler.h>
#include "d3dx12.h"
#include <vector>

#define BUFFER_COUNT 3

enum ResourceState {
	PRESENT,
	RENDER_TARGET,
	SHADER_RESOURCE,
	VERTEX_OR_CONSTANT_BUFFER,
	COPY_DEST,
	READ
};

struct DefaultVertex {
	float pos[4];
	float uvCoordinate[2];
	float normal[4];
	float bitangent[4];
	float tangent[4];
};

struct PointLight {
	float position[4];
	float diffuse[4];
	float att;
	float pad[3];
};

struct ConstantBuffer {
	ID3D12Resource* pBuffer;
	unsigned int heapIndex;

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbViewDesc;
	void* pAddress;
};

struct Texture {
	ID3D12Resource* pTexture;
	ID3D12Resource* pTextureUpload;
	DXGI_FORMAT format;
	D3D12_RESOURCE_STATES resourceState;
};

struct ShaderResource : public Texture {
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc;
	unsigned int heapIndex;
};

struct VertexBuffer {
	ID3D12Resource* vertexBuffer;
	ID3D12Resource* vertexBufferUpload;

	D3D12_VERTEX_BUFFER_VIEW vbView;
};

struct IndexBuffer {
	ID3D12Resource* indexBuffer;
	ID3D12Resource* indexBufferUpload;

	D3D12_INDEX_BUFFER_VIEW ibView;
};

struct RenderTarget {
	ShaderResource* shaderResource;

	unsigned int rtvHeapIndex;
	D3D12_RENDER_TARGET_VIEW_DESC rtvDesc;
};

struct DepthBuffer {
	ShaderResource* shaderResource;

	unsigned int dsvHeapIndex;
	D3D12_DEPTH_STENCIL_VIEW_DESC depthDesc;
};

class DX12Graphics
{
public:
	DX12Graphics();
	~DX12Graphics();

	void Initialize(HWND hWnd, unsigned int width, unsigned int height);
	void SetRootSignature(ID3D12RootSignature*, unsigned int);

	void BindShaderResource(unsigned int bindIndex, ShaderResource* t) {

		if (t->resourceState != D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE) {
			cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(t->pTexture, t->resourceState, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE));
			t->resourceState = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
		}

		CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandleHeap1 = CD3DX12_CPU_DESCRIPTOR_HANDLE(cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(), t->heapIndex, cbvSrvUavDescriptorHandleIncrementSize);
		CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandleHeap2 = CD3DX12_CPU_DESCRIPTOR_HANDLE(cbvSrvUavShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(), cbvSrvUavDrawOffset+bindIndex, cbvSrvUavDescriptorHandleIncrementSize);

		device->CopyDescriptorsSimple(1, cpuHandleHeap2, cpuHandleHeap1, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		resourcesBound++;
	}

	void BindBufferResource(unsigned int bindIndex, ConstantBuffer* t) {

		CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandleHeap1 = CD3DX12_CPU_DESCRIPTOR_HANDLE(cbvSrvUavNonShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(), t->heapIndex, cbvSrvUavDescriptorHandleIncrementSize);
		CD3DX12_CPU_DESCRIPTOR_HANDLE cpuHandleHeap2 = CD3DX12_CPU_DESCRIPTOR_HANDLE(cbvSrvUavShaderVisibleHeap->GetCPUDescriptorHandleForHeapStart(), cbvSrvUavDrawOffset + bindIndex, cbvSrvUavDescriptorHandleIncrementSize);

		device->CopyDescriptorsSimple(1, cpuHandleHeap2, cpuHandleHeap1, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

		resourcesBound++;
	}

	unsigned int GetCurrentBackBufferIndex() { return swapChain->GetCurrentBackBufferIndex(); }
	void WaitForResourceLoads();
	void BeginFrame();
	void SetVertexBuffer(VertexBuffer*);
	void SetIndexBuffer(IndexBuffer*);
	
	RenderTarget* CreateRenderTarget(unsigned int width, unsigned int height, DXGI_FORMAT format);
	DepthBuffer*  CreateDepthBuffer(unsigned int width, unsigned int height, DXGI_FORMAT format);

	void SetRenderTarget(std::vector<RenderTarget*> renderTargets, DepthBuffer* depthBuffer);
	void ClearRenderTarget(std::vector<RenderTarget*> renderTargets, const float* clearColor);

	void ClearDepthStencil(DepthBuffer*);

	void Draw(unsigned int vtxCount, VertexBuffer*, ID3D12PipelineState* pso, ID3D12RootSignature* rootSignature);
	void DrawIndexed(unsigned int idxCount, unsigned int idxStart, VertexBuffer*, IndexBuffer*, ID3D12PipelineState* pso, ID3D12RootSignature* rootSignature);
	void EndFrame();

	ShaderResource* CreateTexture(const char* filename);
	D3D12_SHADER_BYTECODE CompileShader(LPCWSTR vertexFilename, LPCSTR profile);
	ID3D12RootSignature* CreateRootSignature(std::vector<D3D12_ROOT_PARAMETER> rootParameters, std::vector<D3D12_STATIC_SAMPLER_DESC> samplers);
	
	ConstantBuffer* CreateConstantBuffer(unsigned int structureSize);
	void SetData(ConstantBuffer* cb, void* pData, unsigned int size);

	template<typename T>
	VertexBuffer* CreateVertexBuffer(T t, unsigned int byteWidth, unsigned int stride) {
		
		VertexBuffer* vb = new VertexBuffer();

		device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(byteWidth),
			D3D12_RESOURCE_STATE_COPY_DEST,
			nullptr,
			IID_PPV_ARGS(&vb->vertexBuffer));

		device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(byteWidth),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&vb->vertexBufferUpload));

		D3D12_SUBRESOURCE_DATA resource = {};
		resource.pData = t;
		resource.RowPitch = byteWidth;
		resource.SlicePitch = byteWidth;

		UpdateSubresources(cmdList, vb->vertexBuffer, vb->vertexBufferUpload, 0, 0, 1, &resource);
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(vb->vertexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));

		vb->vbView = {};
		vb->vbView.BufferLocation = vb->vertexBuffer->GetGPUVirtualAddress();
		vb->vbView.SizeInBytes = byteWidth;
		vb->vbView.StrideInBytes = stride;

		return vb;
	}

	IndexBuffer* CreateIndexBuffer(bool r32, const unsigned int* indices, unsigned int byteWidth) {

		IndexBuffer* indexBuffer = new IndexBuffer();

		device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(byteWidth),
			D3D12_RESOURCE_STATE_COPY_DEST,
			nullptr,
			IID_PPV_ARGS(&indexBuffer->indexBuffer));

		device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
			D3D12_HEAP_FLAG_NONE,
			&CD3DX12_RESOURCE_DESC::Buffer(byteWidth),
			D3D12_RESOURCE_STATE_GENERIC_READ,
			nullptr,
			IID_PPV_ARGS(&indexBuffer->indexBufferUpload));

		D3D12_SUBRESOURCE_DATA indexData = {};
		indexData.pData = indices;
		indexData.RowPitch = byteWidth;
		indexData.SlicePitch = byteWidth;

		UpdateSubresources(cmdList, indexBuffer->indexBuffer, indexBuffer->indexBufferUpload, 0, 0, 1, &indexData);
		cmdList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(indexBuffer->indexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER));

		indexBuffer->ibView = {};
		indexBuffer->ibView.BufferLocation = indexBuffer->indexBuffer->GetGPUVirtualAddress();
		indexBuffer->ibView.Format = (r32) ? DXGI_FORMAT_R32_UINT : DXGI_FORMAT_R16_UINT;
		indexBuffer->ibView.SizeInBytes = byteWidth;

		return indexBuffer;
	}

	void Render();
public:
	ID3D12Device* device;
	ID3D12GraphicsCommandList* cmdList;
	ID3D12CommandAllocator* cmdAllocator[BUFFER_COUNT];
	ID3D12CommandQueue* cmdQueue;

	HANDLE fenceEvent;
	unsigned long long fenceValue[BUFFER_COUNT];
	ID3D12Fence* fence[BUFFER_COUNT];
	
	unsigned int rtvHeapIndex = 3;
	unsigned int frameIndex;
	unsigned int rtvDescriptorHandleOffset;
	unsigned int dsvHeapOffset;
	unsigned int dsvHeapIndex;
	unsigned int cbvSrvUavHeapIndex;
	unsigned int cbvSrvUavDescriptorHandleIncrementSize;

	unsigned int resourcesBound;
	ID3D12DescriptorHeap* rtvHeap;
	ID3D12DescriptorHeap* dsvHeap;

	ID3D12DescriptorHeap* cbvSrvUavNonShaderVisibleHeap;
	ID3D12DescriptorHeap* cbvSrvUavShaderVisibleHeap;

	ID3D12DescriptorHeap* rtvSVHeap;

	unsigned int cbvSrvUavDrawOffset;

	ID3D12Resource* rtvTextures[BUFFER_COUNT];

	IDXGIFactory1* factory;
	IDXGISwapChain3* swapChain;

	D3D12_VIEWPORT view;
	D3D12_RECT rect;
};

