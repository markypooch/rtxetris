struct VertexIn {
    float4 position : POSITION;
    float2 tex      : TEXCOORD;
};

struct VertexOut {
    float4 position : SV_POSITION;
    float2 tex      : TEXCOORD;
};

VertexOut main(VertexIn vIn) {
    VertexOut vOut = (VertexOut)0;

    vOut.position = vIn.position;
    vOut.tex = vIn.tex;
    return vOut;
}