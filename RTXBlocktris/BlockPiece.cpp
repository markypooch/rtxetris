#include "BlockPiece.h"
#include "Model.h"

BlockPiece::BlockPiece(DX12Graphics* context) {
	wvpConstantBuffer = new ConstantBufferHelper<wvp>(context, 3);
	this->context = context;
}

void BlockPiece::Init(BlockType blockType) {

	switch (blockType) {
	case BlockType::STRAIGHT:
		this->model = Model::LoadAI(*context, "Assets/model/blocktypeone.gltf", 800, 600, {});
		break;
	}
}

void BlockPiece::Update(float dt) {

}