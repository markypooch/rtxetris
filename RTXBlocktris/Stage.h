#pragma once
#include "IStage.h"
#include "ConstantBufferHelper.h"
#include "DX12Graphics.h"
#include "BlockPiece.h"

struct CameraPosition {
	float cameraPosition[4];
};

class Stage : public IStage
{
public:
	Stage(DX12Graphics* context) {
		this->context = context;
	}

	virtual void OnLoad() override;
	virtual void OnUpdate(float dt, const bool*) override;
	virtual void OnDraw() override;

public:

	ShaderResource* reflect;

	float currentRotY;
	float currentRotX;

	float currentPosX = 0.0f;
	float currentPosZ = 2.0f;

	bool coldMouse = true;
	POINT lastPoint;

	Matrix4x4 proj;
	Matrix4x4 view;

	ConstantBufferHelper<wvp>* wvpCB;
	ConstantBufferHelper<CameraPosition>* cameraPositionCB;

	std::vector<RenderTarget*> gbuffer;
	DepthBuffer* gbufferDepth;

	RenderTarget* brightnessExtract;
	DepthBuffer* brightnessExtractDepth;

	ID3D12RootSignature* brightnessExtractRootSignature;
	ID3D12PipelineState* brightnessExtractPso;

	D3D12_SHADER_BYTECODE beVsByteCode, bePsByteCode;

	RenderTarget* bloom;
	DepthBuffer* bloomDepth;

	ID3D12RootSignature* bloomRootSignature;
	ID3D12PipelineState* bloomPso;

	D3D12_SHADER_BYTECODE bloomVsCode, bloomPsCode;

	RenderTarget* texture2D;
	DepthBuffer* texture2DDepth;

	ID3D12PipelineState* texturePso;
	D3D12_SHADER_BYTECODE textureVsCode, texturePsCode;

	RenderTarget* composite;
	DepthBuffer* compositeDepth;

	DX12Graphics* context;
	BlockPiece* blockPiece;

	ID3D12RootSignature* rootSignature;
	ID3D12PipelineState* pso;

	D3D12_SHADER_BYTECODE vsByteCode;
	D3D12_SHADER_BYTECODE psByteCode;

	VertexBuffer* vb;
};

