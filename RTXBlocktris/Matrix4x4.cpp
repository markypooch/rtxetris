#include "Matrix4x4.h"
#include <memory>

Matrix4x4::Matrix4x4() {}
Matrix4x4::~Matrix4x4() {}

Matrix4x4::Matrix4x4(Matrix4x4Data data) {
	memcpy(r, data.r, sizeof(float) * 16);
}

void Matrix4x4::Identity() {
	r[0] = 1.0f;
	r[1] = 0.0f;
	r[2] = 0.0f;
	r[3] = 0.0f;
	r[4] = 0.0f;
	r[5] = 1.0f;
	r[6] = 0.0f;
	r[7] = 0.0f;
	r[8] = 0.0f;
	r[9] = 0.0f;
	r[10] = 1.0f;
	r[11] = 0.0f;
	r[12] = 0.0f;
	r[13] = 0.0f;
	r[14] = 0.0f;
	r[15] = 1.0f;
}

void Matrix4x4::RotateY(float radians) {
	r[0] = cos(radians);
	r[2] = sin(radians);
	
	r[8] = -sin(radians);
	r[10] = cos(radians);
	r[15] = 1.0f;
}

void Matrix4x4::RotateX(float radians) {
	r[0] = 1.0f;
	r[5] = cos(radians);
	r[6] = -sin(radians);
	r[9] = sin(radians);
	r[10] = cos(radians);
	r[15] = 1.0f;
}

Matrix4x4 Matrix4x4::Multiply(Matrix4x4& a, Matrix4x4& b) {

	Matrix4x4Data c;

	c.r[0] = a.r[0] * b.r[0] + a.r[1] * b.r[4] + a.r[2] * b.r[8]   + a.r[3] * b.r[12];
	c.r[1] = a.r[0] * b.r[1] + a.r[1] * b.r[5] + a.r[2] * b.r[9]   + a.r[3] * b.r[13];
	c.r[2] = a.r[0] * b.r[2] + a.r[1] * b.r[6] + a.r[2] * b.r[10]  + a.r[3] * b.r[14];
	c.r[3] = a.r[0] * b.r[3] + a.r[1] * b.r[7] + a.r[2] * b.r[11]  + a.r[3] * b.r[15];

	c.r[4] = a.r[4] * b.r[0] + a.r[5] * b.r[4] + a.r[6] * b.r[8] + a.r[7] * b.r[12];
	c.r[5] = a.r[4] * b.r[1] + a.r[5] * b.r[5] + a.r[6] * b.r[9] + a.r[7] * b.r[13];
	c.r[6] = a.r[4] * b.r[2] + a.r[5] * b.r[6] + a.r[6] * b.r[10] + a.r[7] * b.r[14];
	c.r[7] = a.r[4] * b.r[3] + a.r[5] * b.r[7] + a.r[6] * b.r[11] + a.r[7] * b.r[15];

	c.r[8]  = a.r[8] * b.r[0] + a.r[9] * b.r[4] + a.r[10] * b.r[8] + a.r[11] * b.r[12];
	c.r[9]  = a.r[8] * b.r[1] + a.r[9] * b.r[5] + a.r[10] * b.r[9] + a.r[11] * b.r[13];
	c.r[10] = a.r[8] * b.r[2] + a.r[9] * b.r[6] + a.r[10] * b.r[10] + a.r[11] * b.r[14];
	c.r[11] = a.r[8] * b.r[3] + a.r[9] * b.r[7] + a.r[10] * b.r[11] + a.r[11] * b.r[15];

	c.r[12] = a.r[12] * b.r[0] + a.r[13] * b.r[4] + a.r[14] * b.r[8]  + a.r[15] * b.r[12];
	c.r[13] = a.r[12] * b.r[1] + a.r[13] * b.r[5] + a.r[14] * b.r[9]  + a.r[15] * b.r[13];
	c.r[14] = a.r[12] * b.r[2] + a.r[13] * b.r[6] + a.r[14] * b.r[10] + a.r[15] * b.r[14];
	c.r[15] = a.r[12] * b.r[3] + a.r[13] * b.r[7] + a.r[14] * b.r[11] + a.r[15] * b.r[15];

	Matrix4x4 ret = Matrix4x4(c);

	return ret;
}

void Matrix4x4::Translate(float x, float y, float z) {

	r[12] = x;
	r[13] = y;
	r[14] = z;
}

void Matrix4x4::CreatePerspectiveProjection(float fovy, float aspectRatio, float fNear, float fFar) {
	
	float g = 1.0f / tan(fovy * 0.5f);
	float n = 1.0f / (fFar - fNear);

	r[0] = g / aspectRatio;
	r[1] = 0.0f;
	r[2] = 0.0f;
	r[3] = 0.0f;

	r[4] = 0.0f;
	r[5] = g;
	r[6] = 0.0f;
	r[7] = 0.0f;

	r[8] = 0.0f;
	r[9] = 0.0f;
	r[10] = fFar * n;
	r[11] = 1.0f;

	r[12] = 0.0f;
	r[13] = 0.0f;
	r[14] = -fFar * fNear * n;
	r[15] = 0.0f;
}

Matrix4x4Data Matrix4x4::GetData() {
	
	Matrix4x4Data data;
	std::memcpy(&data.r, &this->r, sizeof(float) * 16);

	return data;
}