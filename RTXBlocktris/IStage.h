#pragma once

class IStage
{
public:
	virtual void OnLoad() = 0;
	virtual void OnUpdate(float dt, const bool*) = 0;
	virtual void OnDraw() = 0;
};

