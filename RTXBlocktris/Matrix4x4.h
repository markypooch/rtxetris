#pragma once

struct Matrix4x4Data {
	float r[16];
};

class Matrix4x4
{
public:
	Matrix4x4();
	Matrix4x4(Matrix4x4Data data);
	~Matrix4x4();
public:
	void Identity();
	void Translate(float x, float y, float z);
	void RotateY(float radians);
	void RotateX(float radians);
	Matrix4x4 Multiply(Matrix4x4& a, Matrix4x4& b);
	void CreatePerspectiveProjection(float fovy, float aspectRation, float fNear, float fFar);
	Matrix4x4Data GetData();
private:
	float r[16];
};

