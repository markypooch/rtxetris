#include "NDCTriangle.h"
#include "Model.h"

struct Vertex {
	float position[4];
	float textureCoord[2];
};

NDCTriangle::NDCTriangle(DX12Graphics* context) {
	this->context = context;
}

void NDCTriangle::Init() {

	Vertex vertices[] = {
		{-0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 1.0f},
		{ 0.0f,  0.5f, 0.5f, 1.0f, 1.0f, 1.0f},
		{ 0.5f, -0.5f, 0.5f, 1.0f, 1.0f, 0.0f}
	};

	vb = context->CreateVertexBuffer(vertices, sizeof(Vertex) * 3, sizeof(Vertex));
	resource = context->CreateTexture("test.png");

	vsCode = context->CompileShader(L"ndc_vs.hlsl", "vs_5_0");
	psCode = context->CompileShader(L"ndc_ps.hlsl", "ps_5_0");

	cb[0] = context->CreateConstantBuffer(sizeof(float) * 4);
	cb[1] = context->CreateConstantBuffer(sizeof(float) * 4);
	cb[2] = context->CreateConstantBuffer(sizeof(float) * 4);

	D3D12_DESCRIPTOR_RANGE ranges[2] = {};
	ranges[0].NumDescriptors = 1;
	ranges[0].BaseShaderRegister = 0;
	ranges[0].OffsetInDescriptorsFromTableStart = 0;
	ranges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	ranges[0].RegisterSpace = 0;

	ranges[1].NumDescriptors = 1;
	ranges[1].BaseShaderRegister = 0;
	ranges[1].OffsetInDescriptorsFromTableStart = 1;
	ranges[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	ranges[1].RegisterSpace = 0;

	D3D12_ROOT_DESCRIPTOR_TABLE table = {};
	table.NumDescriptorRanges = 2;
	table.pDescriptorRanges = ranges;

	D3D12_ROOT_PARAMETER rootParam;
	rootParam.DescriptorTable = table;
	rootParam.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;
	rootParam.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;

	std::vector<D3D12_ROOT_PARAMETER> parameters;
	parameters.push_back(rootParam);
	
	D3D12_STATIC_SAMPLER_DESC sampler = {};
	sampler.AddressU = sampler.AddressV = sampler.AddressW = D3D12_TEXTURE_ADDRESS_MODE_MIRROR;
	sampler.ComparisonFunc = D3D12_COMPARISON_FUNC_NEVER;
	sampler.Filter = D3D12_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	sampler.MaxAnisotropy = 0;
	sampler.MinLOD = 0;
	sampler.MaxLOD = D3D12_FLOAT32_MAX;
	sampler.MipLODBias = 0;
	sampler.RegisterSpace = 0;
	sampler.ShaderRegister = 0;
	sampler.ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	std::vector<D3D12_STATIC_SAMPLER_DESC> samplers;
	samplers.push_back(sampler);

	rootSignature = context->CreateRootSignature(parameters, samplers);

	D3D12_INPUT_ELEMENT_DESC inputE[] = {
		{"POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0},
		{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0}
	};

	D3D12_INPUT_LAYOUT_DESC inputLayout = {};
	inputLayout.NumElements = 2;
	inputLayout.pInputElementDescs = inputE;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = inputLayout;
	psoDesc.VS = vsCode;
	psoDesc.PS = psCode;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.NumRenderTargets = 1;
	psoDesc.pRootSignature = rootSignature;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.SampleMask = 0xffffffff;

	context->device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&pso));
}

void NDCTriangle::Render() {
	unsigned int frameIndex = context->GetCurrentBackBufferIndex();
	color[0] = 2.5f;
	color[1] = 0.5f;
	color[2] = 0.5f;
	color[3] = 0.5f;

	context->SetData(cb[frameIndex], color, sizeof(float) * 4);

	context->SetVertexBuffer(vb);

	context->SetRootSignature(rootSignature, 1);
	context->BindShaderResource(0, resource);
	context->BindBufferResource(1, cb[frameIndex]);
	context->Draw(3, vb, pso, rootSignature);
}